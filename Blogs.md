# Blogs

- Your blog must be posted between the start date/time and end date/time of a particular week.
- You may only post one blog per week (you may post a second if you use a token).
- Your blog must be tagged to appear on the [CS@Worcester blog](http://cs.worcester.edu/).
- Your blog must be tagged with the appropriate week tag as given in the table below.
- Your blog must be tagged with the course number `CS-343`

See the [Professional Development Blog Entry Specification](https://gitlab.com/worcester/cs/kwurst/blog-rubrics/-/blob/master/ProfDevBlogEntrySpec.md) for further details.

Week (tag) | Start Date (at 00:01) | End Date (Due before 23:59)
--- | --- | ---
`Week-1` | 8 September 2024 | 14 September 2024
`Week-2` | 15 September 2024 | 21 September 2024
`Week-3` | 22 September 2024 | 28 September 2024
`Week-4` | 29 September 2024 | 5 October 2024
`Week-5` | 6 October 2024 | 12 October 2024
`Week-6` | 13 October 2024 | 19 October 2024
`Week-7` | 20 October 2024 | 26 October 2024
`Week-8` | 27 October 2024 | 2 November 2024
`Week-9` | 3 November 2024 | 9 November 2024
`Week-10` | 10 November 2024 | 16 November 2024
`Week-11` | 17 November 2024 | 23 November 2024
`Week-12` | 24 November 2024 | 30 November 2024
`Week-13` | 1 December 2024 | 7 December 2024
`Week-14` | 8 December 2024 | 14 December 2024
`Week-15` | 15 December 2024 | 21 December 2024

&copy; 2024 Karl R. Wurst, <kwurst@worcester.edu>

<img src="http://mirrors.creativecommons.org/presskit/buttons/88x31/png/by-sa.png" width=100px/>This work is licensed under the Creative Commons Attribution-ShareAlike 4.0 International License. To view a copy of this license, visit [http://creativecommons.org/licenses/by-sa/4.0/]() or send a letter to Creative Commons, 444 Castro Street, Suite 900, Mountain View, California, 94041, USA.
