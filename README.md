# CS-343 Software Construction, Design and Architecture
> *Software construction techniques and tools, software architectures and frameworks, design patterns, object-oriented design and programming. Efficiency, reliability and maintainability of software.*

**Karl R. Wurst**
<br>Worcester State University

Upon successful completion of this course, students will be able to:
* Apply a wide variety of software construction techniques and tools, including state-­‐based and table-­‐driven approaches to low-­‐level design of software
* Take requirements for simple systems and develop software architectures and high-­‐level designs
* Apply a variety of design patterns, frameworks, and architectures in designing a wide variety of software
* Perform object-­‐oriented design and programming with a high level of proficiency
* Analyze and modify software in order to improve its efficiency, reliability, and maintainability