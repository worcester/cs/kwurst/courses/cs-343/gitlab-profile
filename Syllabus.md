# CS-343 Software Construction, Design and Architecture

## CS-343 01, 02 &mdash; Fall 2024

### Version 2024-Fall-1.10, 6 September 2024

## Credit and Contact Hours

3 credits

Lecture: 3 hours/week

## Catalog Course Description

> *Software construction techniques and tools, software architectures and frameworks, design patterns, object-oriented design and programming. Efficiency, reliability and maintainability of software.*

## Instructor

Dr. Karl R. Wurst

I prefer to be addressed, or referred to, as "Professor Wurst" or "Doctor Wurst". My pronouns are "he/him/his", although I am also fine with "they/them/their".

See [http://karl.w-sts.com](http://karl.w-sts.com) for contact information and schedule.

## Meeting Times and Locations

Section | Time | Location
--- | --- | ---
01 | TR 11:30-12:45 | ST 107
02 | MW 12:30-13:45 | ST 107

## It's in the Syllabus

**If you don't find the answer to your question in the syllabus, then please ask me.**

![It's in the syllabus comic](http://www.phdcomics.com/comics/archive/phd051013s.gif)
[http://www.phdcomics.com/comics.php?f=1583](http://www.phdcomics.com/comics.php?f=1583)

## Textbook

<!-- markdownlint-disable MD033 -->
<table cellpadding="1" border="0">
<tbody>
<tr>
<td align="center"><a title="By Michael Reschke (self-made, following OERCommons.org&#039;s example) [Public domain], via Wikimedia Commons" href="https://commons.wikimedia.org/wiki/File%3AOERlogo.svg"><img width="100" alt="OERlogo" src="https://upload.wikimedia.org/wikipedia/commons/thumb/9/90/OERlogo.svg/256px-OERlogo.svg.png"/></a>
</td>
<td>We will be using freely available learning resources for topics in this course.
</td>
</tr>
</tbody>
</table>
<!-- markdownlint-enable MD033 -->

## Required Materials

In addition to the textbook, to successfully complete this course you will need:

1. **Laptop Computer:** You will need a laptop computer that you can bring to class sessions and can use at home. **You must bring your laptop to every class session.** The brand, specs, and operating system (Windows, Mac OS X, Linux) are unimportant – we will be using a cloud development environment.
2. **Internet Access:** You will need Internet access for access to:
   1. **Blackboard** – All course announcements, grades, and links to assignments and course materials will be made available through the course site on Blackboard.
   2. **WSU Gmail** &mdash; You must check your WSU Gmail account on a regular basis.
       * All communications made directly to individual students, such as notifications of missing assignments, or comments and needed corrections on submitted assignments will be sent to your WSU Gmail as notifications from Blackboard or GitLab.
       * All Blackboard announcements to the class, such as corrections to assignments or changes in due dates, will also be sent to your WSU Gmail account as notifications.

       **Checking your WSU Gmail on a regular basis is the best way to make sure you do not miss important, time-sensitive information from me between class sessions.**
   3. **GitLab** – Most assignments (in-class and outside of class) will be posted on and submitted through GitLab.
   4. **GitPod** – We will using GitPod to provide a cloud development environment with all the tools needed for in-class and homework assignments. You will need a GitPod account, which provides 50 hours of free development time per month.
   5. **Tutorials and articles** – I will suggest, and you will research on your own, tutorials and articles for you to learn new technologies and techniques we need.

## Where Does This Course Lead?

* CS 448 Software Development Capstone
* Your professional career

## Course Workload Expectations

***This is a three-credit course. You should expect to spend, on average, 9 hours per week on this class.***

You will spend 3 hours per week in class. In addition, you should expect to spend, on average, at least 6 hours per week during the semester outside of class. (See *Definition of the Credit Hour*)

## Definition of the Credit Hour

>The Commission has adopted the federal definition of a credit hour: an amount of work represented in intended learning outcomes and verified by evidence of student achievement that is consistent with commonly accepted practice in postsecondary education and that reasonably approximates not less than –
>
>(1) One hour of classroom or direct faculty instruction and a minimum of two hours of out of class student work each week for approximately fifteen weeks for one semester or trimester hour of credit, or ten to twelve weeks for one quarter hour of credit, or the equivalent amount of work over a different amount of time; or
>
>(2) At least an equivalent amount of work as required in paragraph (1) of this definition for other academic activities as established by the institution including laboratory work, internships, practica, studio work, and other academic work leading to the award of credit hours.
>
>In determining the amount of work associated with a credit hour, the institution may take into account a variety of delivery methods, measurements of student work, academic calendars, disciplines, and degree levels.
>
>&mdash; New England Commission of Higher Education, [Policy on Credits and Degrees](https://www.neche.org/wp-content/uploads/2021/07/Pp111-Policy-on-Credits-and-Degrees-REVISED.pdf)

## Prerequisites

This course has a prerequisite of CS 286 – Database Design and Applications. I expect that you understand the fundamentals of database structure and queries.

This course has a prerequisite or corequisite of CS 348 - Software Process Management. I expect that you have had experience using a version control system to fork/clone, add/commit, and push/pull, and how to use a build system. CS-348 has prerequisite of CS-140, and so I expect that you are competent at writing programs that contain moderate numbers of classes,that you can write and execute simple unit tests, write clear, well documented code, and use an appropriate development environment.

***If you are missing any of this background, you should not take this course.***

## Course-Level Student Learning Outcomes

Upon successful completion of this course, students will be able to:

* Apply a wide variety of software construction techniques and tools, including state-based and table-­driven approaches to low-­level design of software
* Take requirements for simple systems and develop software architectures and high-­level designs
* Apply a variety of design patterns, frameworks, and architectures in designing a wide variety of software
* Perform object-­oriented design and programming with a high level of proficiency
* Analyze and modify software in order to improve its efficiency, reliability, and maintainability

## LASC Student Learning Outcomes

This course does not fulfill any LASC Content Area requirements, but contributes to the following Overarching Outcomes of LASC:

* Demonstrate effective oral and written communication.
* Employ quantitative and qualitative reasoning.
* Apply skills in critical thinking.
* Apply skills in information literacy.
* Understand the roles of science and technology in our modern world.
* Understand how scholars in various disciplines approach problems and construct knowledge.
* Display socially responsible behavior and act as socially responsible agents in the world.
* Make connections across courses and disciplines

## Software Development Concentration Student Learning Outcomes

This course addresses the following outcomes of the Software Development Concentration of the Computer Science Major:

Graduates of the Software Development Concentration will be able to (in addition to the Computer Science Major Program Learning Outcomes):

1. Work with stakeholders to specify, design, develop, test, modify, and document a software system. (Emphasis/Mastery)
2. Organize, plan, follow, and improve on, appropriate software development methodologies and team processes for a software project. (Introduction)
3. Evaluate, select, and use appropriate tools for source code control, build, test, deployment, and documentation management. (Introduction)
4. Evaluate, select, and apply appropriate testing techniques and tools, develop test cases, and perform software reviews. (Introduction)
5. Apply professional judgement, exhibit professional behavior, and keep skills up-to-date. (Mastery)

## Program-Level Student Learning Outcomes

This course addresses the following [Program Outcomes for the Major in Computer Science](https://catalog.worcester.edu/undergraduate/school-education-health-natural-sciences/computer-science/major-computer-science/):

Upon successful completion of the Major in Computer Science, students will be able to:

* Analyze a problem, develop/design multiple solutions, and evaluate and document the solutions based on the requirements. (Emphasis)
* Communicate effectively both in written and oral form. (Emphasis)
* Identify professional and ethical considerations and apply ethical reasoning to technological solutions to problems. (Emphasis)
* Demonstrate an understanding of and appreciation for the importance of negotiation, effective work habits, leadership, and good communication with teammates and stakeholders. (Emphasis)
* Learn new models, techniques, and technologies as they emerge and appreciate the necessity of such continuing professional development. (Emphasis)

## Course Topics

The topics covered in this course will be selected from (not all will be covered):

* Design Principles
  * Object Oriented Programming
    * Abstraction
    * Encapsulation
    * Polymorphism
    * Inheritance
  * SOLID
    * Single Responsibility Principle (SRP)
    * Open-Closed Principle (OCP)
    * Liskov Substitution Principle (LSP)
    * Interface Segregation Principle (ISP)
    * Dependency Inversion Principle (DIP)
  * DRY (Don't Repeat Yourself)
  * YAGNI (You Ain't Gonna Need It)
  * GRASP (General Responsibility Assignment Software Patterns)  
    * Controller
    * Creator
    * High Cohesion
    * Indirection
    * Information Expert
    * Low Coupling
    * Polymorphism
    * Protected Variations
    * Pure Fabrication
  * "Encapsulate what varies."
  * "Program to an interface, not an implementation."
  * "Favor composition over inheritance."
  * "Strive for loosely coupled designs between objects that interact"
  * Principle of Least Knowledge (AKA Law of Demeter)
  * Inversion of Control
  * Design Patterns
    * Creational
    * Structural
    * Behavioral
* Concurrency
* Refactoring
* Smells
  * Code Smells
  * Design Smells
* Software Architectures
  * Architectural Patterns
  * Architectural Styles
* REST API Design
* Software Frameworks
* Documentation
* Modeling
  * Unified Modeling Language (UML)
  * C4 Model
* Anti-Patterns
* Implementation of Web Systems
  * Front end
  * Back end
  * Data persistence layer

## Instructional Methods

This class will not be a traditional “lecture” class, and will incorporate some teaching methods that may be unfamiliar to you.

### POGIL

Rather than lecturing about the course content, you and your classmates will "discover" the content for yourselves through small group work.

The group work will be a very structured style called Process Oriented Guided Inquiry Learning (POGIL). Through investigation of models of the concepts and answering questions that guide the team toward understanding of the models, your team will learn both the content and team process skills. In your POGIL groups each group member will have a specific role to play during the activity, and roles will be rotated so that everyone will get to experience a variety of process skills.

For more information on POGIL, see [What is POGIL?](https://pogil.org/about-pogil/what-is-pogil).

### HFOSS Kits

Rather than learning and practicing course content and technology using small, "made-up" examples and projects, we will use existing Humanitarian Free and Open Source Software projects. This will let you practice within an authentic, real-world context.

These Kits have been created from projects that have been "frozen" at a particular point in time, to allow specific student activities and to allow you, your classmates, and the instructor to take on the roles of the developers and maintainers of the projects.

### Competency- and Specification-Based Grading

See *Grading Policies* below.

## Grading Policies

I want everyone receiving a passing grade in this course to be, at least, minimally competent in the course learning outcomes and for that to be reflected in your course grade. Traditional grading schemes do a poor job of indicating competency.

As an example, imagine a course with two major learning outcomes: X and Y. It is widely considered that a course grade of C indicates that a student is minimally competent in achieving the course outcomes. However, if the student were to receive a grade of 100 for outcome X, and a grade of 40 for outcome Y, the student would still have a 70 (C-) average for the course. Yet the student is clearly not competent in outcome Y.

Therefore the grading in this course will be handled in a different manner:

### Assignment Grading

All assignments will be graded on a 3-point scale, based on how well the
student's work meets the instructor-supplied specification:

Description | Score
--- | ---
Clearly meets specification | 3 pts
Key portions incorrect or missing | 2 pts
Clearly does not meet specification | 1 pt
No work submitted | 0 pts

* **For each assignment, you will be given a detailed specification explaining
what is expected.**
* If you are unclear on what the specification requires, it is your
responsibility to ask me for clarification.
* For assignments with a score of 2 or 1, you have the opportunity to increase
the score on that assignment by 1 point by completing an
***Assignment Self-Reflection Report*** (see below.)

#### Assignment Self-Reflection Report

When assignments are graded, an assignment solution will be posted. If your
score on an assignment is a 2 or a 1, you have the opportunity to raise your
score by 1 point by completing an *Assignment Self-Reflection Report*.

In an *Assignment Self-Reflection Report* you will carefully compare the
posted solution for the assignment to your submitted assignment.

* Explain how and why the posted solution differs from your assignment.
  * What did you not understand that caused your assignment to not meet the
  specification?
  * You must do this for each difference you find.
* Reflect on your process for completing the assignment.
  * Why did your assignment not meet the specification? For example:
    * Did you not understand the specification?
    * Did you miss class concepts needed for your assignment to meet the
    specification?
    * Did you not test your solution completely enough?
    * Etc.
* Identify two things that you will do differently in the future to address the reasons that your assignment did not meet the specifications.
* How many previous assignments have not fully met the specification. How is this assignment similar or different?

***Assignment Self-Reflection Reports must be submitted within 7 days after the original assignment grade was posted.***

### Course Grade Determination

Your grade for the course will be determined by which assignments and/or
how many assignments you complete, meeting each assignment's specification.

* A minimum collection of assignments, indicating competency in most of the course learning outcomes, must be completed to specification to earn a passing course grade (D).
* A minimum collection of assignments, indicating competency in all of the course learning outcomes, must be completed to specification to earn a course grade of C.
* Higher passing grades (A and B) can be earned by completing more assignments and/or assignments that show higher-level thinking and learning skills.

#### Base Grade

<!-- markdownlint-disable MD033 -->
Assignment | Earn Base Grade<br>A | Earn Base Grade<br>B | Earn Base Grade<br>C | Earn Base Grade<br>D
--- | :-: | :-: | :-: | :-:
**Class Attendance and Participation** (out of 23 classes) | 21 (91%) | 19 (83%) | 17 (74%) | 15 (65%)
**Assignments** (out of 6 assignments)
&nbsp;&nbsp;&mdash; **Base Assignments** (out of 18 points) | 17 points | 16 points| 15 points | 12 points
&nbsp;&nbsp;&mdash; **Intermediate "Add-On"** (maximum of 3 add-on attempts will count) |  7 points | 5 points| |
&nbsp;&nbsp;&mdash; **Advanced "Add-On"** (maximum of 2 add-on attempts will count) | 5 points| 3 points |  |
**Self-Directed Professional Development Blog Entries** (out of 14 weeks) | 4 | 3 | 2 | 1
**Exam Grade Average** (3 exams) | > 50% | > 50% | > 50% | &le; 50%
<!-- markdownlint-enable MD033 -->

* **Failing to meet the all the requirements for a particular letter grade will result in not earning that grade.** For example, even if you complete all other requirements for a B grade, but write only 2 "Self-Directed Professional Development Blog Entries" that meet the specification you will earn a C grade.
* **Failing to meet the all the requirements for earning a D grade will result in a failing grade for the course.**

#### Plus or Minus Grade Modifiers

* You will have a ***minus*** modifier applied to your base grade if the average of your exam grades is 65% or lower.
* You will have a ***plus*** modifier applied to your base grade if the average of your exam grades is 85% or higher.

*Notes:*

* WSU has no A+ grade.
* I reserve the right to revise *downward* the required number of assignments needed for each base grade due to changes in number of assignments assigned or unexpected difficulties with assignments.

## Attendance and Participation

Because a significant portion of your learning will take place during the in-class activities and as part of your team, class attendance and participation are extremely important.

For your attendance and participation to meet specification, you must:

* Arrive on time and stay for the entire class session.
* Participate fully in the in-class activity:
  * Fulfill all responsibilities of your team role.
  * Contribute to the team's learning and answers to questions.
  * Work as part of the team on the activity (not on your own.)
  * Work on the in-class activity (not some other work.)

## Assignments

The assignments will give you a chance to apply the material to different or larger tasks. The assignments will vary in what you will be asked to do - design, programming projects, written assignments, analysis, etc.

### Base Assignment

Every assignment will have a *base assignment* portion which represents the
minimum level of work to show competency in that material.

### Intermediate "Add-On"

Most assignments will also have an *Intermediate Add-On* portion that can be
completed for anyone working for a course grade of B or higher. This will
involve more detailed work on the same topic.

*You may apply **at most 3** Intermediate Add-On attempts to your Base Grade.*

### Advanced "Add-On"

Most assignments will also have an *Advanced Add-On* portion that can be
completed for anyone working for a course grade of B or higher. This will
involve even more detailed work on the same topic.

*You may apply **at most 2** Advanced Add-On attempts to your Base Grade.*

## Reading Assignments

In addition, there will be regular, assigned reading assignments. You are expected to complete these reading assignments by their assigned due dates, but you will not be checked on this. Completing these reading assignments will be needed to successfully complete the assignments and exam questions.

## Exams

We will have three exams. The exams will be completed outside of class time.

The first two exams will be posted on a Thursday, and will be due on the following Tuesday by 23:59. You may complete the exam in any 2-hour period you wish during that time.

* Exam 1 is tentatively scheduled to be posted on Thursday, 10 October 2024, and due before 23:59 on Tuesday, 15 October 2024.
* Exam 2 is tentatively scheduled to be posted on Thursday, 14 November 2024 and due before 23:59 on Tuesday, 19 November 2024.
* Exam 3 will be posted on Wednesday, 11 December 2024 and will be due on by 23:59 on the last day of the Final Exam period: Friday, 20 December 2024. *If you wish to take the exam during the scheduled final exam time for ANY of the sections, I will be available at that time ***by advance request*** to answer questions.*

## Self-Directed Professional Development Blog

Two of the CS Program-Level Student Learning Outcomes that this course addresses are:

> * Learn new models, techniques, and technologies as they emerge and appreciate the necessity of such continuing professional development. (Mastery)
> * Communicate effectively both in written and oral form. (Mastery)

You will be required to read outside blogs, articles, and/or books; listen to podcasts; watch video tutorials/lectures; etc. on your own and keep a blog about those items that you found useful/interesting. Your blog must be publicly accessible[^1], and will be aggregated on the [CS@Worcester Blog](http://cs.worcester.edu/).

* Differing numbers of blog entries are required for different passing grades. See the table under *Course Grade Determination*.
* I will only accept one blog entry per week. **You cannot wait until the end of the semester and then turn in all of your blog entries.**

[^1]: If there is a reason why your name cannot be publicly associated with this course, you may blog under a pseudonym. You must see me to discuss the details, but your blog must still be publicly accessible and aggregated, and you must inform me of your pseudonym.

## Deliverables

All work will be submitted electronically through a variety of tools. The due date and time will be given on the assignment. The submission date and time will be determined by the submission timestamp of the tool used.

**Please do not submit assignments to me via email.** It is difficult for me to keep track of them and I often fail to remember that they are in my mailbox when it comes time to grade the assignment.

## Late Submissions

Unless otherwise specified, work is due by 9 AM ET on the day it is due.

If submitting work late, the following policy applies:

* Less than 72 hours late - No penalty.
* More than 72 hours late - Not accepted.

Quizzes and exams must be completed on time, otherwise they receive a 0.

## Revision and Resubmission of Self-Directed Professional Development Blog Entries

You may revise and resubmit the ***first*** blog entry on which you receive a ***Does Not Yet Meet Specification*** without the use of a token.

* You must have submitted the original blog entry on time, (or within the 72 hour grace period.)
* You may ask me for clarification of the comments I made on your blog entry.
* You may ask me to look at your revised blog entry to see if it addresses my comments.
* If you address all the comments in an acceptable fashion, your grade will be converted to ***Meets Specification***.
* You must let me know by email when you have posted the revised blog entry (**with the URL to the revised blog entry**), so that I know to regrade it.

## Tokens

Each student will be able to earn up to 5 tokens over the course of the semester. These tokens will be earned by completing simple set-up and housekeeping tasks for the course.

Each token can be used to:

* replace a single missed class session (up to a maximum of 2 missed class sessions)
* turn in a second blog entry in an a single week
* revise a blog entry beyond the first that does not meet specification
* Each unused token remaining at the end of the semester can be used to increase the exam average by 2 percentage points.

### Token Accounting

* Unused tokens will be kept track of in the Blackboard *My Grades* area.
* Tokens will not be automatically applied. You must explicitly tell me **by email** when you want to use a token, and for which assignment.

## Getting Help

If you are struggling with the material or a project please see me as soon as possible. Often a few minutes of individual attention is all that is needed to get you back on track.

By all means, try to work out the material on your own, but ask for help when you cannot do that in a reasonable amount of time. The longer you wait to ask for help, the harder it will be to catch up.

**Asking for help or coming to see me during office hours is not bothering or annoying me. I am here to help you understand the material and be successful in the course.**

## Contacting Me

You may contact me by email (kwurst@worcester.edu), telephone (+1-508-929-8728), or see me in my office. My office hours are listed on the schedule on my web page ([http://karl.w-sts.com](http://karl.w-sts.com)) or you may make an appointment for a mutually convenient time.

**If you email me, please include “[CS-343]” in the subject line, so that my email program can correctly file your email and ensure that your message does not get buried in my general mailbox.**

**If you email me from an account other than your Worcester State email, please be sure that your name appears somewhere in the email, so that I know who I am communicating with.**

My goal is to get back to you within 24 hours of your email or phone call (with the exception of weekends and holidays), although you will likely hear from me much sooner. If you have not heard from me after 24 hours, please remind me.

![Cartoon with bad examples of how to send email to your instructor](http://www.phdcomics.com/comics/archive/phd042215s.gif)
[http://www.phdcomics.com/comics.php?f=1795](http://www.phdcomics.com/comics.php?f=1795)

## Code of Conduct/Classroom Civility

All students are expected to adhere to the policies as outlined in the University's Student Code of Conduct.

## Student Responsibilities

* Contribute to a class atmosphere conducive to learning for everyone by asking/answering questions, participating in class discussions. Don’t just lurk!
* When working with a partner, participate actively. Don't let your partner do all the work - you won't learn anything that way.
* Seek help when necessary
* Start assignments as soon as they are posted.  Do not wait until the due date to seek help/to do the assignments
* Make use of the student support services (see below)
* Expect to spend at least 9 hours of work per week on classwork.
* Each student is responsible for the contents of the readings, handouts, and homework assignments.

## Preferred Names and Pronouns

If you prefer to be addressed, or referred to, by a different name than appears on your student record and/or specify your preferred pronouns, please let me know.

If you wish this information to appear on class lists for all of your professors, you may wish to complete the [Student Chosen Name, Gender Identity, and Pronoun Usage Request Form](https://www.google.com/url?client=internal-element-cse&cx=009351802468954661311:z3yt6xnre90&q=https://www.worcester.edu/WorkArea/DownloadAsset.aspx%3Fid%3D13439&sa=U&ved=2ahUKEwjNlsyIqpLyAhXOMVkFHX29DCoQFjAAegQIBxAB&usg=AOvVaw1TtxrjeStu5KS6pRyuxh8m) and return it to the Registrar's Office.

## Additional Institutional Information

Academic Affairs maintains documents with additional information about policies and services available to students. You may access them [here](https://drive.google.com/drive/folders/1rB4aoG-367T3-ytk5WUbFckodHGdqyv4).

## Academic Conduct

Each student is responsible for the contents of the readings, discussions, class materials, textbook and handouts. All work must be done independently unless assigned as a group project. You may discuss assignments and materials with other students, but you should never share answers or files. **Everything that you turn in must be your own original work, unless specified otherwise in the assignment.**

Students may help each other understand the programming language and the development environment but students may not discuss actual solutions, design or implementation, to their programming assignments before they are submitted or share code or help each other debug their programming assignments. The assignments are the primary means used to teach the techniques and principles of computer programming; only by completing the programs individually will students receive the full benefit of the assignments. If you are looking at each other’s code before you submit your own, you are in violation of this policy.

Students may not use solutions to assignments from any textbooks other than the text assigned for the course, or from any person other than the instructor, or from any Internet site, or from any other source not specifically allowed by the instructor. If a student copies code from an unauthorized source and submits it as a solution to an assignment, the student will receive a 0 for that assignment.

**Any inappropriate sharing of work or use of another's work without attribution will result in a grade of zero on that assignment for all parties involved. If you do so a second time, you will receive an “E” for the course.**

Academic integrity is an essential component of a Worcester State education. Education is both the acquisition of knowledge and the development of skills that lead to further intellectual development. Faculty are expected to follow strict principles of intellectual honesty in their own scholarship; students are held to the same standard. Only by doing their own work can students gain the knowledge, skills, confidence and self-worth that come from earned success; only by learning how to gather information, to integrate it and to communicate it effectively, to identify an idea and follow it to its logical conclusion can they develop the habits of mind characteristic of educated citizens. Taking shortcuts to higher or easier grades results in a Worcester State experience that is intellectually bankrupt.

Academic integrity is important to the integrity of the Worcester State community as a whole. If Worcester State awards degrees to students who have not truly earned them, a reputation for dishonesty and incompetence will follow all of our graduates. Violators cheat their classmates out of deserved rewards and recognition. Academic dishonesty debases the institution and demeans the degree from that institution.  

It is in the interest of students, faculty, and administrators to recognize the importance of academic integrity and to ensure that academic standards at Worcester State remain strong. Only by maintaining high standards of academic honesty can we protect the value of the educational process and the credibility of the institution and its graduates in the larger community.

**You should familiarize yourself with Worcester State College’s Academic Honesty policy. The policy outlines what constitutes academic dishonesty, what sanctions may be imposed and the procedure for appealing a decision. The complete Academic Honesty Policy appears at: [http://www.worcester.edu/Academic-Policies/](http://www.worcester.edu/Academic-Policies/)**

**If you have a serious problem that prevents you from finishing an assignment on time, contact me and we'll come up with a solution.**

## Student Work Retention Policy

It is my policy to securely dispose of student work one calendar year after grades have been submitted for a course.

## Schedule

**This schedule is subject to change.**

### Section 01 - TR 11:30-12:45

<!-- markdownlint-disable MD033 -->
Date | Class | Exams
:-: | :-: | :-:
Tuesday, 3 September | **&#9940; No Class**<br>Pre-College Conference |
Thursday, 5 September | *Introduction, Team Roles, and Grading Schemes*
Tuesday, 10 September | *Object-Oriented Design Principles*
Thursday, 12 September | *Modeling with UML Class Diagrams*
Tuesday, 17 September | *Modeling with UML Sequence Diagrams*
Thursday, 19 September | *Design Smells and Technical Debt*
Tuesday, 24 September | **&#9940; No Class**<br>Unity Day
Thursday, 26 September | *Design Patterns*
Tuesday, 1 October | *Design Patterns*
Thursday, 3 October | *SOLID Principles*
Tuesday, 8 October | *Microservice Architecture*
Thursday, 10 October | *Introduction to REST APIs* | **&#128175; Exam 1** will be posted on Thursday, 10 October 2024
Tuesday, 15 October | *Exploring REST API Calls* | **&#128175; Exam 1** will be due by 23:59 on Tuesday, 15 October 2024
Thursday, 17 October | *Semantic Versioning*
Tuesday, 22 October | *Specifying REST API Calls*
Thursday, 24 October | *Specifying REST API Calls*
Tuesday, 29 October | *Implementing REST API Calls*
Thursday, 31 October | *Docker*
Tuesday, 5 November | **&#9940; No Class**<br>I have a medical appointment
Thursday, 7 November | *Implementing REST API Calls*
Tuesday, 12 November | *Simple Frontend*
Thursday, 14 November | *Simple Frontend* | **&#128175; Exam 2** will be posted on Thursday, 14 November 2024
Tuesday, 19 November | *Exploring Frontend Implementation*  | **&#128175; Exam 2** will be due by 23:59 on Tuesday, 19 November 2024
Thursday, 21 November | *Exploring Frontend Implementation*
Tuesday, 26 November | *Implementing New Frontend Code*
Thursday, 28 November | **&#9940; No Class**<br>Thanksgiving Recess
Tuesday, 3 December | *Implementing New Frontend Code*
Thursday, 5 December | *Slack, because topics above will take longer than planned* &#128521;
Tuesday, 10 December | **&#9940; No Class**<br>Professional Development Day | **&#128175; Exam 3** will be posted on Wednesday, 11 December 2024
Thursday, 12 December | **&#9940; No Class**<br>Final Exams
Tuesday, 17 December | **&#9940; No Class**<br>Final Exams
Thursday, 19 December | **&#9940; No Class**<br>Final Exams | **&#128175; Exam 3** will be due by 23:59 on Friday, 20 December 2024

### Section 02 - MW 12:30-13:45

<!-- markdownlint-disable MD033 -->
Date | Class | Exams
:-: | :-: | :-:
Monday, 2 September | **&#9940; No Class**<br>Labor Day
Wednesday, 4 September | *Introduction, Team Roles, and Grading Schemes*
Monday, 9 September | *Object-Oriented Design Principles*
Wednesday, 11 September | *Modeling with UML Class Diagrams*
Monday, 16 September | *Modeling with UML Sequence Diagrams*
Wednesday, 18 September | *Design Smells and Technical Debt*
Monday, 23 September | **&#9940; No Class**<br>I have a medical appointment
Wednesday, 25 September | *Design Patterns*
Monday, 30 September | *Design Patterns*
Wednesday, 2 October | *SOLID Principles*
Monday, 7 October | *Microservice Architecture*
Wednesday, 9 October | *Introduction to REST APIs* | **&#128175; Exam 1** will be posted on Thursday, 10 October 2024
Monday, 14 October | **&#9940; No Class**<br>Indigenous Peoples' Day | **&#128175; Exam 1** will be due by 23:59 on Tuesday, 15 October 2024
Wednesday, 16 October | *Exploring REST API Calls*
Monday, 21 October | *Semantic Versioning*
Wednesday, 23 October | *Specifying REST API Calls*
Monday, 28 October | *Specifying REST API Calls*
Wednesday, 30 October | *Implementing REST API Callss*
Monday, 4 November | *Docker*
Wednesday, 6 November | *Implementing REST API Calls*
Monday, 11 November | **&#9940; No Class**<br>Veterans' Day
Wednesday, 13 November | *Simple Frontend* | **&#128175; Exam 2** will be posted on Thursday, 14 November 2024
Monday, 18 November | *Simple Frontend* | **&#128175; Exam 2** will be due by 23:59 on Tuesday, 19 November 2024
Wednesday, 20 November | *Exploring Frontend Implementation* 
Monday, 25 November | *Exploring Frontend Implementation*
Wednesday, 27 November | **&#9940; No Class**<br>Thanksgiving Recess
Monday, 2 December | *Implementing New Frontend Code*
Wednesday, 4 December | *Implementing New Frontend Code*
Monday, 9 December | **&#9940; No Class**<br>Reading Day
Wednesday, 11 December | **&#9940; No Class**<br>Final Exams | **&#128175; Exam 3** will be posted on Wednesday, 11 December 2024
Monday, 16 December | **&#9940; No Class**<br>Final Exams
Wednesday, 18 December | **&#9940; No Class**<br>Final Exams | **&#128175; Exam 3** will be due by 23:59 on Friday, 20 December 2024

<!-- markdownlint-enable MD033 -->

&copy; 2024 Karl R. Wurst, <kwurst@worcester.edu>

<!-- markdownlint-disable MD033 -->
<img src="http://mirrors.creativecommons.org/presskit/buttons/88x31/png/by-sa.png" width=100px/>This work is licensed under the Creative Commons Attribution-ShareAlike 4.0 International License. To view a copy of this license, visit [http://creativecommons.org/licenses/by-sa/4.0/](http://creativecommons.org/licenses/by-sa/4.0/) or send a letter to Creative Commons, 444 Castro Street, Suite 900, Mountain View, California, 94041, USA.
<!-- markdownlint-enable MD033 -->